// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database. 
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const emailsRef = functions.database.ref('/emails/{userId}/{emailId}');
const SENDGRID_API_KEY = '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';

exports.sendEmail = emailsRef.onWrite(event => {
  // using SendGrid's v3 Node.js Library
  // https://github.com/sendgrid/sendgrid-nodejs
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(SENDGRID_API_KEY);
  // Grab the current value of what was written to the Realtime Database.
  const writtenData = event.data.val();
  console.log(writtenData);
  const { from, to, message } = writtenData;
  const msg = {
    to, from,
    subject: `Village des érables - covoiturage: ${from}`,
    text: `from: ${from}, message: ${message}`,
    html: `<p><strong>from:</strong> ${from}</p> 
           <p><strong>message:</strong> ${message}</p>`
  };
  return Promise.resolve().then(()=> {
    return sgMail.send(msg)
                .then(() => {      
                    console.log(`email send from ${from} to ${to}`);      
                })
  });
});