import { Component, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/subscription';
import { HelpService } from './help.service';
import * as service from '../services/services.barrel';

@Component({
  selector: 'vde-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  providers: [HelpService]
})
export class HelpComponent implements OnDestroy {

  isBusy = false;
  helps = [];
  translations;
  getDataSubscription: Subscription;
  languageSubscription: Subscription;
  translationsSubscription: Subscription;

  constructor(
    private helpService: HelpService,
    private translationsService: service.TranslationsService,
    private ngZone: NgZone) {
    this.getTranslations();
    this.observeLanguageChange();
    this.initLanguage();
  }

  ngOnDestroy() {
    this.unsubscribeObservables();
  }

  initLanguage(): void {
    this
      .translationsService
      .language$
      .next(this.translationsService.language);
  }

  observeLanguageChange(): void {
    this.languageSubscription =
      this.translationsService
        .language$
        .subscribe(language => {
          this.getData(language);
        })
  }

  unsubscribeObservables(): void {
    if (this.getDataSubscription) this.getDataSubscription.unsubscribe();
    if (this.languageSubscription) this.languageSubscription.unsubscribe();
    if (this.translationsSubscription) this.translationsSubscription.unsubscribe();
  }

  getData(language: string): void {
    this.isBusy = true;
    this.getDataSubscription =
      this
        .helpService
        .getData(language)
        .subscribe((helps: any[]) => {
          this.ngZone.run(() => {
            this.helps = helps;
            this.isBusy = false;
          });
        })
  }

  getTranslations(): void {
    this.translationsSubscription = 
      this.translationsService
          .translations$
          .subscribe(translations => {
            this.translations = translations;
          });
  }

}
