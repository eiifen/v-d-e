import { Injectable } from '@angular/core';
import * as service from '../services/services.barrel';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HelpService {

  constructor(
    private firebaseService: service.FirebaseService,
    private utilitiesService: service.UtilitiesService
  ) { }

  getData(language: string): Observable<any[]> {
    return Observable.fromEvent(
      this.firebaseService
        .database()
        .ref(`help/${language}`), 'value')
        .map((snapshot: any) => snapshot.val())
        .map((help: any) => this.toArray(help))
        .map((help: any[]) => this.sortByOrder(help));
  }

  toArray(help): any[] {
    let list = [];
    if (!help) return list;

    Object
      .keys(help)
      .forEach((helpId: string) => {
        list.push(help[helpId]);
      });

    return list;
  }

  sortByOrder(help: any[]): any[] {
    return this.utilitiesService
      .sortBy(help, helpObject => helpObject.order);
  }
}