import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HelpComponent } from './help.component';
import { ToolbarTitleModule } from './../shared/toolbar-title/toolbar-title.module';
import { SharedModule } from './../shared/shared.module';

const routes: Routes = [
  { path: '',  component: HelpComponent }  
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    ToolbarTitleModule
  ],
  declarations: [HelpComponent]
})
export class HelpModule { }
