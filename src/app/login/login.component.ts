import { Component, AfterViewInit, NgZone } from '@angular/core';
import * as service from '../services/services.barrel';

@Component({
  selector: 'vde-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  readonly firebaseWidgetElementID: string = 'firebaseui-auth-container';
  appTitle: string;

  constructor(
    private ngZone: NgZone,
    private firebaseService: service.FirebaseService) { 
      this.getAppTitle();
    }

  ngAfterViewInit() {
    this.initFirebaseUI();
  }

  private initFirebaseUI(): void {
    let uiConfig = {
      signInSuccessUrl: '/posts',
      signInOptions: [...this.firebaseService.getProviders()],
      tosUrl: '/terms' // Terms of service url.
    };
    // Initialize the FirebaseUI Widget using Firebase.
    this.firebaseService.firebaseUIWidget.start(`#${this.firebaseWidgetElementID}`, uiConfig);
  }

  getAppTitle(): void {
    this
      .firebaseService
      .database()
      .ref('meta-data/app-title')
      .on('value', snapshot => {
        this.ngZone.run(() => {
          this.appTitle = snapshot.val();
        });
      })

  }

}
