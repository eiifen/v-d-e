import { Injectable } from '@angular/core';

@Injectable()
export class ErrorMessageService {

  getMessage(type: string, fieldName: string = 'Field'): string {
    switch (type) {
      case 'required':
        return `${fieldName} is required`;

      case 'email':
        return `Please enter a valid email address`;

      case 'captcha':
        return `Please answer correctly. (Spam control)`;
    }
  }

  generateErrorMessageObject(errorConfig: any[]): any {
    let object = errorConfig.reduce(
      (accumulator, currentValue) => {
        let type = currentValue.type;
        let fieldName = currentValue.fieldName;      
        accumulator[`${fieldName}${type}`] = this.getMessage(type, fieldName);
        return accumulator;
      }, {});

    return object;
  }

}