import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class MomentService {

  formatObj = {
    primary: 'D MMM YYYY' // 25 sept 2017
  }

  constructor() { }

  nowISO(): string {
    return moment().toISOString();
  }

  format(dateISO:string, format:string = this.formatObj.primary): string {
    return moment(dateISO).format(format);
  }
}