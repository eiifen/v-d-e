import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { auth, database, User } from 'firebase/index';
import * as firebaseui from 'firebaseui';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

interface IFirebaseUser {
  displayName?: string;
  email?: string;
  emailVerified?: boolean;
  photoURL?: string;
  userId?: string;
}

@Injectable()
export class FirebaseService {

  readonly firebaseUIWidget: any;
  user: User;
  user$ = new BehaviorSubject({});
  auth: auth.Auth = auth();
  database = database;
  isAuthenticated$ = new BehaviorSubject(false);

  constructor() {   
    this.firebaseUIWidget = this.newFirebaseUIWidget(this.auth);
    this.observeAuthState(this.auth);
  }  

  observeAuthState(auth: auth.Auth = this.auth) {
    auth.onIdTokenChanged((user: User) => {
      this.user = user;
      this.user$.next(user);
      
      if (user) {
        // User is signed in.
        this.isAuthenticated$.next(true);
      } else {
        // User is signed out.        
        this.isAuthenticated$.next(false);
      }
    }, error => {
      console.error(error);
    });    
  }

  getProviders(): Array<string> {
    return [
      // Leave the lines as is for the providers you want to offer your users.      
      auth.GoogleAuthProvider.PROVIDER_ID,
      /* auth.FacebookAuthProvider.PROVIDER_ID,
      auth.TwitterAuthProvider.PROVIDER_ID,
      auth.GithubAuthProvider.PROVIDER_ID, */
      auth.EmailAuthProvider.PROVIDER_ID,
      /* auth.PhoneAuthProvider.PROVIDER_ID */
    ];
  }

  private newFirebaseUIWidget(auth: auth.Auth = this.auth): any {
    return new firebaseui.auth.AuthUI(auth);
  }

  getCurrentUser(auth: auth.Auth = this.auth): User {
    return auth.currentUser;
  }

  getCurrentUserId(auth: auth.Auth = this.auth): string {
    if (auth && auth.currentUser && auth.currentUser.uid) return auth.currentUser.uid;    
    return '';
  }

  getBasicUserInfo(user: User = this.user): IFirebaseUser {
    if (!user) return {};

    return {
      displayName: user.displayName,
      email: user.email,
      emailVerified: user.emailVerified,
      photoURL: user.photoURL,
      userId: user.uid
    } as IFirebaseUser
  }

  query(path: string, once: boolean = false): Observable<any> {
    let observable = Observable
      .fromEvent(this.database().ref(path), 'value')
      .map((snapshot: any) => snapshot.val());
    return once ? observable.take(1) : observable;
  }

  signOut(auth: auth.Auth = this.auth): Promise<any> {
    return new Promise((resolve, reject) => {
      auth
        .signOut()
        .then(() => resolve())
        .catch(error => reject(error))
    });
  }
}

