import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

@Injectable()
export class SnackBarService {

  config = new MatSnackBarConfig();
  readonly duration: number = 1500;

  constructor(
    private MatSnackBar: MatSnackBar
  ) {
    this.config.duration = this.duration; 
   }

  open(message: string = 'success', config: MatSnackBarConfig = this.config): MatSnackBarRef<SimpleSnackBar> {
    return this.MatSnackBar.open(message, null, config);
  }
}