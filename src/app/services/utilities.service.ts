import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable()
export class UtilitiesService {

  truncate(input: string, length: number = 30): string {
    return _.truncate(input, { length });
  }

  sortBy(collection: any[], func: Function) {
    return _.sortBy(collection, [func]);
  }

}
