import { UtilitiesService } from './utilities.service';

describe('UtilitiesService', () => {
  let service = new UtilitiesService();  

  describe('truncate', ()=> {
    it('should return 30 characters by default and last three are ...', ()=>{
      let input = '0123456789 0123456789 0123456789 0123456789';
      let output = service.truncate(input);
      expect(output.length).toBe(30);
      expect(output).toBe('0123456789 0123456789 01234...');
    });

    it('should return 40 characters and last three are ...', ()=>{
      let input = '0123456789 0123456789 0123456789 0123456789';
      let output = service.truncate(input, 40);
      expect(output.length).toBe(40);
      expect(output).toBe('0123456789 0123456789 0123456789 0123...');
    });

  });  
});
