import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FirebaseService } from './firebase.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class TranslationsService {

  language = 'fr';
  language$ = new BehaviorSubject(this.language);
  translations$ = new BehaviorSubject({});

  constructor(private firebaseService: FirebaseService) {
    this.getTranslations();    
  }

  getTranslations(language: string = this.language): Observable<Object> {
    this.firebaseService
      .database()
      .ref(`/translations/${language}`)
      .on('value',
      snapshot => {
        this.translations$.next(snapshot.val());
      },
      error => Observable.throw(error)
      );

    return this.translations$;
  }

  toggleLanguage(): Promise<string> {
    this.language = this.language === 'fr' ? 'en' : 'fr';
    this.language$.next(this.language);
    return Promise.resolve(this.language);
  }

  setLanguageText(): string {
    return this.language === 'fr' ? 'english' : 'français';
  }

}