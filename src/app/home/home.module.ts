import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { PostsListModule } from './../posts/posts-list/posts-list.module';
import { AddPostButtonModule } from './../posts/add-post-button/add-post-button.module';

const routes: Routes = [
  { path: '',  component: HomeComponent }  
];

@NgModule({
  imports: [
    SharedModule,    
    RouterModule.forChild(routes),
    PostsListModule,
    AddPostButtonModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
