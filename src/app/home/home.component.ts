import { Component, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/subscription';

import { PostsService } from './../posts/posts.service';
import * as service from '../services/services.barrel';
import { PostDialogComponent } from '../posts/post-dialog/post-dialog.component';

@Component({
  selector: 'vde-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [PostsService]
})
export class HomeComponent implements OnDestroy {

  isBusy = false;
  isAuthenticated$: Observable<boolean>;
  posts = [];
  getPostsSubscription: Subscription;
  translationsSubscription: Subscription;  
  translations;

  constructor(
    private postsService: PostsService,
    private router: Router,
    private ngZone: NgZone,
    private matDialog: MatDialog,
    private firebaseService: service.FirebaseService,
    private translationsService: service.TranslationsService) {
    this.observeAuthChange();
    this.getTranslations();
    this.getPosts();
  }

  ngOnDestroy() {
    this.unsubscribeObservables();
  }

  unsubscribeObservables(): void {
    if (this.getPostsSubscription) this.getPostsSubscription.unsubscribe();
    if (this.translationsSubscription) this.translationsSubscription.unsubscribe();    
  }

  getPosts(): void {
    this.isBusy = true;
    this.getPostsSubscription =
      this.postsService
        .getPosts()
        .subscribe((posts: any[]) => {
          this.ngZone.run(() => {
            this.posts = posts;
            this.isBusy = false;
          })
        });
  }

  observeAuthChange(): void {
    this.isAuthenticated$ = this.firebaseService.isAuthenticated$;
  }

  getTranslations(): void {
    this.translationsSubscription =
      this.translationsService
        .translations$
        .subscribe((translations: Object) => {
          this.ngZone.run(() => {
            this.translations = translations;
          });
        })
  }

  navigateToLogin(): void {
    this.router.navigate(['/login']);
  }
  
  openAddDialog(): void {
    let matDialogRef = this.matDialog.open(PostDialogComponent, {
      width: '80vw',
      disableClose: true,
      data: { action: 'add' }
    });
  }

}
