import { NgModule } from '@angular/core';
import { 
  MatButtonModule,
  MatMenuModule,
  MatDialogModule,
  MatInputModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatListModule,
  MatToolbarModule,
  MatCardModule,
  MatExpansionModule,
  MatProgressBarModule
} from '@angular/material';

@NgModule({  
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatListModule,
    MatToolbarModule,
    MatCardModule,
    MatExpansionModule,
    MatProgressBarModule
  ]
})
export class MaterialDesignModule { }