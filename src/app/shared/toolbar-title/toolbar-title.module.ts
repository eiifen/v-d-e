import { NgModule } from '@angular/core';

import { SharedModule } from './../shared.module';
import { ToolbarTitleComponent } from './toolbar-title.component';

@NgModule({
  imports: [SharedModule],
  exports: [ToolbarTitleComponent],
  declarations: [ToolbarTitleComponent]
})
export class ToolbarTitleModule { }
