import { Component, Input } from '@angular/core';

@Component({
  selector: 'vde-toolbar-title',
  templateUrl: './toolbar-title.component.html',
  styleUrls: ['./toolbar-title.component.scss']
})
export class ToolbarTitleComponent {
  @Input() title:string;
}
