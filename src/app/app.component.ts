import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as service from './services/services.barrel';

@Component({
  selector: 'vde-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  appTitle$: Observable<string>;
  isAuthenticated$: Observable<boolean>;
  isTranslating = false;
  language$: Observable<string>;
  showUIWhenNotLogin$: Observable<boolean>;
  translations$: Observable<Object>;

  constructor(
    private firebaseService: service.FirebaseService,
    private translationsService: service.TranslationsService,
    private router: Router) {
    this.observeRouterUrl();
    this.observeAuthchange();
    this.getTranslations();
    this.observeAppTitle();
  }

  getTranslations(): void {
    this.language$ = this.translationsService.language$;
    this.isTranslating = true;
    this.translations$ = this.translationsService
      .getTranslations()
      .do(() => {
        this.isTranslating = false;
      })
  }

  private observeAuthchange(): void {
    this.isAuthenticated$ = this.firebaseService.isAuthenticated$;
    this.firebaseService.observeAuthState();
  }

  private observeRouterUrl(): void {
    this.showUIWhenNotLogin$ =
      this.router
        .events
        .filter(event => event instanceof NavigationEnd)
        .map((event: NavigationEnd) => event.url)
        .map((url: string) => {
          return !this.isUrl(url, '/login');
        })        
  }

  toggleLanguage(): void {
    this.translationsService.toggleLanguage();
    this.getTranslations();
  }

  observeAppTitle(): void {
    this.appTitle$ = this.firebaseService.query('meta-data/app-title');
  }

  /* HELPERS */
  isUrl(url: string, pattern: string): boolean {
    return url.includes(pattern) ? true : false;
  }

}
