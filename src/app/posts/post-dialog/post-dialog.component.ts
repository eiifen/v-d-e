import { Component, Inject, NgZone, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IPost } from './post.model';
import { PostsService } from '../posts.service';
import * as service from '../../services/services.barrel';
import { Subscription } from 'rxjs/subscription';

@Component({
  selector: 'vde-post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.scss']
})
export class PostDialogComponent implements OnDestroy {

  form: FormGroup;
  dialogTitle = '';
  buttonSaveText = '';
  isBusy = false;
  translations;
  translationsSubscription: Subscription;

  constructor(
    public matDialogRef: MatDialogRef<PostDialogComponent>,    
    private ngZone: NgZone,
    private formBuilder: FormBuilder,
    private postsService: PostsService,
    private translationsService: service.TranslationsService,
    private snackBarService: service.SnackBarService,
    @Inject(MAT_DIALOG_DATA) public data: any) {        
    this.createForm(this.data.model);
    this.getTranslations();
  }

  ngOnDestroy() {
    this.unsubscribeObservables();
  }

  unsubscribeObservables(): void {
    if (this.translationsSubscription) this.translationsSubscription.unsubscribe();
  }

  onSubmit(model: any): void {
    this.isBusy = true;
    if (this.data.action === 'update') {
      this.postsService
        .update(Object.assign(this.data.model, model))
        .then(() => this.success())
        .catch((error: any) => this.error(error.message || error));
    }
    else {
      this.postsService
        .create(model)
        .then(() => this.success())
        .catch((error: any) => this.error(error.message || error));
    }
  }

  success(): void {
    this.ngZone.run(() => {
      this.isBusy = false;
      this.snackBarService.open('success');
      this.matDialogRef.close();
    });
  }

  error(error: any): void {
    this.ngZone.run(() => {
      this.isBusy = false;
      this.snackBarService.open(error.message || error);
    });
  }

  createForm(model: IPost): void {
    this.form = this.formBuilder.group({
      title: [model ? model.title : '', Validators.required],
      description: [model ? model.description : '', Validators.required]
    });
  }

  private setDialogTitle(action: string, translations: any): string {
    switch (action) {
      case 'update':
        return translations['edit_post'];
      default:
        return translations['add_post'];
    };
  }

  private setButtonSaveText(action: string, translations: any): string {
    switch (action) {
      case 'update':
        return translations['save'];
      default:
        return translations['add'];
    };
  }

  getTranslations(): void {
    this.translationsSubscription =
      this.translationsService
        .getTranslations()
        .subscribe(translations => {
          this.ngZone.run(()=>{
            this.dialogTitle = this.setDialogTitle(this.data.action, translations);
            this.buttonSaveText = this.setButtonSaveText(this.data.action, translations);
            this.translations = translations;            
          });          
        });
  }
}
