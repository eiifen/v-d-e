import { NgModule } from '@angular/core';
import { PostDialogComponent } from './post-dialog.component';
import { SharedModule } from '../../shared/shared.module';
import { PostsService } from './../posts.service';

@NgModule({
  imports: [SharedModule],
  entryComponents: [PostDialogComponent],
  declarations: [PostDialogComponent],
  providers: [PostsService]
})
export class PostDialogModule { }
