import { Component, OnDestroy, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/subscription';
import { MatDialog } from '@angular/material';

import * as service from '../services/services.barrel';
import { PostDialogComponent } from './post-dialog/post-dialog.component';
import { PostsService } from './posts.service';


@Component({
  selector: 'vde-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnDestroy {
  
  posts = [];
  isBusy = false;
  translations;
  user;
  userId$: Observable<string>;
  photoURL$: Observable<string>;
  userSubscription: Subscription;
  translationsSubscription: Subscription;

  constructor(
    private firebaseService: service.FirebaseService,
    private translationsService: service.TranslationsService,
    private postsService: PostsService,
    private ngZone: NgZone,
    private matDialog: MatDialog) {    
    this.doGetPosts();
    this.getTranslations();
  }

  doGetPosts(): void {
    this.isBusy = true;
    this.user = this.getUser();
    this.userSubscription =
        this
        .getPosts(this.user.uid)
        .subscribe((posts: any[]) => {
          this.ngZone.run(() => {
            this.posts = posts;
            this.isBusy = false;
          });
        })
  }

  ngOnDestroy() {
    this.unsubscribeObservables();
  }

  unsubscribeObservables(): void {
    if (this.userSubscription) this.userSubscription.unsubscribe();
    if (this.translationsSubscription) this.translationsSubscription.unsubscribe();
  }

  getUser():any {
    return this.firebaseService.getCurrentUser();
  }

  getPosts(userId: string): Observable<any> {
    return this.postsService.getPostsByUser(userId);
  }

  getTranslations(): void {
    this.translationsSubscription =
      this.translationsService
        .translations$
        .subscribe(translations => {
          this.ngZone.run(()=>{
            this.translations = translations;
          });
        });
  }

  openAddDialog(): void {
    this.ngZone.run(()=>{
      let matDialogRef = this.matDialog.open(PostDialogComponent, {
        width: '80vw',
        disableClose: true,
        data: { action: 'add' }
      });
    });    
  }
}
