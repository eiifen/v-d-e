import { NgModule } from '@angular/core';
import { ReplyDialogComponent } from './reply-dialog.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  entryComponents: [ReplyDialogComponent],
  declarations: [ReplyDialogComponent]
})
export class ReplyDialogModule { }
