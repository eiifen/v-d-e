import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IReply } from './reply.model';
import * as service from '../../services/services.barrel';
import * as myValidator from '../../validators/validators.barrel';

/* declare var jQuery: any; */

@Component({
  selector: 'vde-reply-dialog',
  templateUrl: './reply-dialog.component.html',
  styleUrls: ['./reply-dialog.component.scss'],
  providers: [service.CaptchaService]
})
export class ReplyDialogComponent implements OnInit {

  number1: number;
  number2: number;
  private total: number;
  form: FormGroup;
  errorMessageObject = {};
  isBusy = false;
  @ViewChild('replyform') replyForm: ElementRef;

  constructor(
    public matDialogRef: MatDialogRef<ReplyDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private MatDialog: MatDialog,
    private formBuilder: FormBuilder,
    private captchaService: service.CaptchaService,
    private utilitiesService: service.UtilitiesService,
    private firebaseService: service.FirebaseService,
    private snackBarService: service.SnackBarService,
    private momentService: service.MomentService,    
    private errorMessageService: service.ErrorMessageService) {

    this.generateCaptcha();
    this.createForm();
    this.setErrorObject();
  }

  ngOnInit() {

  }
  /* ngAfterViewInit(){
    jQuery(this.replyForm.nativeElement).captcha();    
  } */

  generateCaptcha() {
    this.number1 = this.captchaService.rndmNr1;
    this.number2 = this.captchaService.rndmNr2;
    this.total = this.captchaService.totalNr;
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, myValidator.EmailValidator()]],
      message: ['', Validators.required],
      captchaAnswer: ['', [Validators.required, myValidator.CaptchaValidator(this.total)]]
    });
  }

  setErrorObject(): void {
    this.errorMessageObject = this.errorMessageService.generateErrorMessageObject([
      { fieldName: 'message', type: 'required' },
      { fieldName: 'email', type: 'required' },
      { fieldName: 'email', type: 'email' },
      { fieldName: 'captchaAnswer', type: 'required' },
      { fieldName: 'captchaAnswer', type: 'captcha' }
    ]);
  }

  onSubmit(model: any): void {
    let { email, uid } = this.data;
    this.showBusy();
    this.firebaseService
      .database()
      .ref(`/emails/${uid}`)
      .push({
        from: model.email,
        to: email,
        message: model.message,
        dateCreated: this.momentService.nowISO()
      })
      .then(
      this.handleSuccess.bind(this),
      this.handleError.bind(this)
      );
  }

  showBusy(): void {
    this.isBusy = true;
  }

  hideBusy(): void {
    this.isBusy = false;
  }

  handleSuccess(): void {
    this.hideBusy();
    this.snackBarService.open();
    this.closeDialog();
  }

  handleError(error): void {
    this.hideBusy();
    this.snackBarService.open(error.message || error);
    this.closeDialog();
  }

  closeDialog(): void {
    this.matDialogRef.close();
  }

}
