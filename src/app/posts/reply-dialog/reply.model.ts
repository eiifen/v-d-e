export interface IReply {
  email: string;
  message: string;
}