import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as service from '../services/services.barrel';
import { APP_CONST } from '../../app/shared/app.constant';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PostsService {

  BLANK_AVATAR = '../../assets/images/ic_account_circle_black_48dp_2x.png';
  user;
  posts$ = new Subject<any[]>();

  constructor(
    private firebaseService: service.FirebaseService,
    private momentService: service.MomentService
  ) {
    this.setUser();
  }

  setUser(): void {
    this.user = this.firebaseService.getCurrentUser();
  }

  create(data): Promise<any> {
    let { uid } = this.user;
    let postsByUserDataModel = this.preparePostsByUserDataModel(data);

    return this
      .createPostByUser(postsByUserDataModel, uid)
      .then(refObject => {
        let { key } = refObject;
        let postsDataModel = this.preparePostDataModel(postsByUserDataModel, this.user);
        return this.createPost(key, postsDataModel);
      });
  }

  update(data): Promise<any> {
    let { uid } = this.user;
    let { postId, title, description } = data;
    let updates = {};
    updates[`posts/${postId}/title`] = title;
    updates[`posts/${postId}/description`] = description;
    updates[`postsByUser/${uid}/${postId}/title`] = title;
    updates[`postsByUser/${uid}/${postId}/description`] = description;
    return this.firebaseService.database().ref().update(updates);
  }

  preparePostsByUserDataModel(data): any {
    let { title, description } = data;
    let model = {
      dateCreated: this.momentService.nowISO(),
      title,
      description
    };
    return model;
  }

  preparePostDataModel(model, user): any {
    let { uid, displayName, email, photoURL } = user;
    let avatarUrl = this.getAvatarURL(photoURL);
    return Object.assign(model, {
      uid, displayName, email,
      photoURL: avatarUrl
    });
  }

  createPostByUser(data, uid): any {
    let { title, description } = data;
    let model = {
      dateCreated: this.momentService.nowISO(),
      title,
      description
    }
    return this.firebaseService
      .database()
      .ref(`/postsByUser/${uid}`)
      .push(model);
  }

  createPost(newKey, model): Promise<any> {
    return this.firebaseService
      .database()
      .ref(`/posts/${newKey}`)
      .set(model);
  }

  getPosts(): Observable<any[]> {
    return Observable
    .fromEvent(
    this.firebaseService
      .database()
      .ref(`posts`), 'value')
    .map((snapshot: any) => this.toArray(snapshot.val()))
    .map((listOfPosts: any[]) => this.sortByLastAdded(listOfPosts));
  }

  getPostsByUser(userId: string): Observable<any> {
    return Observable
      .fromEvent(
      this.firebaseService
        .database()
        .ref(`postsByUser/${userId}`), 'value')
        .map((snapshot: any) => this.toArray(snapshot.val()))
        .map((listOfPosts: any[]) => this.sortByLastAdded(listOfPosts));
  }

  deletePostByUser(postId: string, userId: string): Promise<any> {
    let path = `postsByUser/${userId}/${postId}`;
    return this.firebaseService
      .database()
      .ref(path)
      .remove();
  }

  deletePost(postId: string): Promise<any> {
    let path = `posts/${postId}`;
    return this.firebaseService
      .database()
      .ref(path)
      .remove();
  }

  toArray(object): any[] {
    if (!object) return [];
    let list = [];
    Object.keys(object).forEach((key: string) => {
      let post = object[key];
      post.dateCreatedFormated = this.formatDateCreated(post.dateCreated);
      list.push(Object.assign(post, { postId: key }));
    });
    return list;
  }

  formatDateCreated(dateCreated: string): string {
    return this.momentService.format(dateCreated);
  }

  sortByLastAdded(list: any[]): any[] {
    return list.reverse();
  }

  getAvatarURL(photoURL: string, defaultAvatarUrl: string = this.BLANK_AVATAR): string {
    return photoURL || defaultAvatarUrl;
  }

}