import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ReplyDialogComponent } from './../reply-dialog/reply-dialog.component';
import { PostDialogComponent } from './../post-dialog/post-dialog.component';
import { DeletePostDialogComponent } from '../delete-post-dialog/delete-post-dialog.component';
import { PostsService } from './../posts.service';

@Component({
  selector: 'vde-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent {

  @Input() posts = [];
  @Input() isPostsByUser: boolean = false;
  @Input() photoURL: string;
  @Input() userId: string;
  @Input() translations;
  dialogConfig = { width: '80vw' };

  constructor(
    private matDialog: MatDialog,
    private postsService: PostsService
  ) { }

  openReplyDialog(post: any): void {
    let { email, uid } = post;
    let config = Object.assign({}, this.dialogConfig, { 
      data: { email, uid } 
    });
    let matDialogRef = this.matDialog.open(ReplyDialogComponent, config);
  }

  openConfirmDeleteDialog(postId: string, userId: string): void {
    let config = Object.assign({}, this.dialogConfig, { 
      data: { postId, userId } 
    });
    let matDialogRef = this.matDialog.open(DeletePostDialogComponent, config);
  }

  openUpdatePostDialog(post, userId: string): void {
    let config = 
      Object.assign({}, this.dialogConfig, {
          data: { model: post, action: 'update' }
        });

    let matDialogRef = this.matDialog.open(PostDialogComponent, config);
  }

  scrollToTop(): void {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
  }

}
