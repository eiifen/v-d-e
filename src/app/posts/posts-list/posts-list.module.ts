import { NgModule } from '@angular/core';

import { ReplyDialogModule } from './../reply-dialog/reply-dialog.module';
import { PostDialogModule } from './../post-dialog/post-dialog.module';
import { DeletePostDialogModule } from '../delete-post-dialog/delete-post-dialog.module';
import { PostsListComponent } from './posts-list.component';
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    ReplyDialogModule,
    DeletePostDialogModule,
    PostDialogModule
  ],
  exports: [PostsListComponent],
  declarations: [PostsListComponent]
})
export class PostsListModule { }
