import { NgModule } from '@angular/core';
import { DeletePostDialogComponent } from './delete-post-dialog.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  entryComponents: [DeletePostDialogComponent],
  declarations: [DeletePostDialogComponent]
})
export class DeletePostDialogModule { }
