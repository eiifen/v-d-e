import { Component, Inject, OnDestroy, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/subscription';
import * as service from '../../services/services.barrel';

import { PostsService } from './../posts.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'vde-delete-post-dialog',
  templateUrl: './delete-post-dialog.component.html',
  styleUrls: ['./delete-post-dialog.component.scss']
})
export class DeletePostDialogComponent implements OnDestroy {

  deleteSubscription: Subscription;
  translationsSubscription: Subscription;
  translations;
  isBusy = false;

  constructor(
    public matDialogRef: MatDialogRef<DeletePostDialogComponent>,
    private postsService: PostsService,
    private snackBarService: service.SnackBarService,
    private translationsService: service.TranslationsService,
    private ngZone: NgZone,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.getTranslations();
    }

  ngOnDestroy() {
    this.unsubscribeObservables();
  }

  unsubscribeObservables(): void {
    if (this.deleteSubscription) this.deleteSubscription.unsubscribe();
    if (this.translationsSubscription) this.translationsSubscription.unsubscribe();
  }

  delete(): void {
    let { postId, userId } = this.data;
    this.isBusy = true;
    this.deleteSubscription =
      Observable
        .fromPromise(this.postsService.deletePostByUser(postId, userId))
        .switchMap(() => this.postsService.deletePost(postId))
        .subscribe(
          this.success.bind(this),
          error => this.error(error.message || error)
        );
  }

  success(): void {
    this.closeDialog();
    this.snackBarService.open('success');
  }

  error(error: string): void {
    this.closeDialog();
    this.snackBarService.open(error);
  }

  closeDialog(): void {
    this.matDialogRef.close();
  }

  getTranslations(): void {
    this.translationsSubscription =
      this.translationsService
        .getTranslations()
        .subscribe(translations => {
          this.ngZone.run(()=>{
            this.translations = translations;
          });
        });
  }
}
