import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostsComponent } from './posts.component';
import { SharedModule } from '../shared/shared.module';
import { PostsListModule } from './posts-list/posts-list.module';
import { AddPostButtonModule } from './add-post-button/add-post-button.module';
import { ToolbarTitleModule } from './../shared/toolbar-title/toolbar-title.module';

const routes: Routes = [
  { path: '',  component: PostsComponent }  
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),    
    PostsListModule,
    AddPostButtonModule,
    ToolbarTitleModule
  ],
  declarations: [PostsComponent]
})
export class PostsModule { }
