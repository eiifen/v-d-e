import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'vde-add-post-button',
  templateUrl: './add-post-button.component.html',
  styleUrls: ['./add-post-button.component.scss']
})
export class AddPostButtonComponent {

  @Input() isAuthenticated = true;
  @Output() openAddDialog = new EventEmitter();
  @Output() navigateToLogin = new EventEmitter();

  openAddPostDialog(): void {
    if (!this.isAuthenticated) {
      this.navigateToLogin.emit();
    }
    else {
      this.openAddDialog.emit();
    }
  }

}
