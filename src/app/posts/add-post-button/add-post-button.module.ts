import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { AddPostButtonComponent } from './add-post-button.component';

@NgModule({
  imports: [SharedModule],
  exports: [AddPostButtonComponent],
  declarations: [AddPostButtonComponent]  
})
export class AddPostButtonModule { }
