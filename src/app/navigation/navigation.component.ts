import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'vde-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  @Input() appTitle: string;
  @Input() translations;
  @Output() toggleSidenav = new EventEmitter();
}
