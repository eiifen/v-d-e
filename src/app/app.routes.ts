import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'login', loadChildren: 'app/login/login.module#LoginModule' },    
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule' },  
  { path: 'posts', loadChildren: 'app/posts/posts.module#PostsModule', canActivate: [AuthGuard] },  
  { path: 'profile', loadChildren: 'app/profile/profile.module#ProfileModule', canActivate: [AuthGuard] },
  { path: 'help', loadChildren: 'app/help/help.module#HelpModule' },
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
