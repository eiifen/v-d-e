import { AbstractControl, ValidatorFn } from '@angular/forms';

export function CaptchaValidator(total: number): ValidatorFn {

  return (control: AbstractControl): { [key: string]: any } => {
    let controlValue: any = control.value;
    let errorObject = {
      'captcha': {
        value: controlValue
      }
    };

    const pattern = /^\d+$/;
    if (!controlValue) return null;
    if (!pattern.test(controlValue)) return errorObject;
    return +controlValue === total ? null : errorObject;

  };
}