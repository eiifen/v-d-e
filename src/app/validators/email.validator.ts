import { AbstractControl, ValidatorFn } from '@angular/forms';

export function EmailValidator(): ValidatorFn {
  
  return (control: AbstractControl): { [key: string]: any } => {
    let controlValue: any = control.value;
    let errorObject = {
      'email': {
        value: controlValue
      }
    };

    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;    
    if (!controlValue) return null;    
    return pattern.test(controlValue) ? null : errorObject;

  };
  
}