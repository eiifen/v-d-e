import { PostsService } from './posts/posts.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// material design: Gesture Support
import 'hammerjs';

import * as services from './services/services.barrel';
import './shared/rxjs.barrel';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { NavigationModule } from './navigation/navigation.module';
import { SidenavModule } from './sidenav/sidenav.module';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    NavigationModule,
    SidenavModule
  ],
  providers: [
    AuthGuard,
    services.FirebaseService,
    services.UtilitiesService,
    services.SnackBarService,
    services.ErrorMessageService,
    services.MomentService,
    services.TranslationsService,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
