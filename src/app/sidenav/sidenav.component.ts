import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import * as $ from 'jquery';
import * as service from '../services/services.barrel';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/subscription';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'vde-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent implements OnChanges {
  languageText: string;
  @Input() language: string;
  @Input() translations;
  @Input() isTranslating: boolean;
  @Input() isAuthenticated: boolean;
  @Output() toggleLanguage = new EventEmitter();
  @ViewChild('sidenav') sidenav: ElementRef;

  constructor(
    private router: Router,
    private firebaseService: service.FirebaseService,
    private translationsService: service.TranslationsService
  ) { }

  ngOnChanges() {
    if (this.language && this.translations) {
      this.languageText = this.language === 'fr' ? this.translations['en'] : this.translations['fr'];
    }
  }

  doToggleLanguage(): void {
    this.toggleLanguage.emit();
    this.toggleSidenav();
  }

  toggleSidenav() {
    let $sidenav = $(this.sidenav.nativeElement);
    $('html, body').scrollTop(0);
    $sidenav.toggleClass('open');
  }

  signIn(): void {
    this.navigateTo('/login')
  }

  signOut(): void {
    this
      .firebaseService
      .signOut()
      .then(() => {
        this.navigateTo('/home');
      })
      .catch(error => console.error(error));
  }

  navigateTo(path: string): void {
    this.toggleSidenav();
    if (path) {
      this.router.navigate([path]);
    } else {
      this.router.navigate(['/home']);
    }
  }

}
