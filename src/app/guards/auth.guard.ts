import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private firebaseService: FirebaseService,
    private router: Router
  ) { }

  canActivate(): Observable<boolean> {
    this.firebaseService.observeAuthState();
    
    return  this.firebaseService
                .isAuthenticated$            
                .take(1)                
                .do((isAuthenticated: boolean) => { 
                  if (!isAuthenticated) {
                    this.router.navigate(['/login']);                  
                  }
                });                              
          }
}