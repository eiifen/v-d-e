export interface IProfile {
  uid: string;
  displayName: string;
  email: string;  
  phoneNumber: string;  
}