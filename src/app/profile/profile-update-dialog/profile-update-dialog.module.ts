import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ProfileUpdateDialogComponent } from './profile-update-dialog.component';

@NgModule({
  imports: [SharedModule],
  entryComponents: [ProfileUpdateDialogComponent],
  declarations: [ProfileUpdateDialogComponent]
})
export class ProfileUpdateDialogModule { }
