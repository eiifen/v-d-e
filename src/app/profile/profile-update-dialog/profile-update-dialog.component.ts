import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmailValidator } from '../../validators/email.validator';
import * as service from '../../services/services.barrel';


@Component({
  selector: 'vde-profile-update-dialog',
  templateUrl: './profile-update-dialog.component.html',
  styleUrls: ['./profile-update-dialog.component.scss']
})
export class ProfileUpdateDialogComponent {

  isEmailForm: boolean = false;
  form: FormGroup;
  dialogTitle: string;
  buttonSaveText: string;
  errorMessageObject = {};

  constructor(
    public dialogRef: MatDialogRef<ProfileUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private MatDialog: MatDialog,
    private formBuilder: FormBuilder,
    private errorMessageService: service.ErrorMessageService) {

    let field = data.field,
      value = data.value;
    this.createForm(field, value);
    this.dialogTitle = this.setDialogTitle(field);
    this.setFormVisibility(field);
    this.setErrorObject();
  }


  open(): void {
    this.MatDialog.open(ProfileUpdateDialogComponent, {
      width: '80vw'
    })
  }

  private createForm(field: string, value: string): void {
    let form: FormGroup;
    switch (field) {
      case 'email':
        form = this.formBuilder.group({
          email: [value || '', [Validators.required, EmailValidator()]]
        });
        break;

      default:
        form = this.formBuilder.group({
          displayName: [value || '', Validators.required]
        });
        break;
    }

    this.form = form;
  }

  private setDialogTitle(field: string): string {
    switch (field) {
      case 'email':
        return 'Edit Email';
      default:
        return 'Edit Name';
    };
  }

  setFormVisibility(field: string): void {
    this.isEmailForm = field === 'email' ? true : false;
  }

  setErrorObject(): void {    
    this.errorMessageObject = this.errorMessageService.generateErrorMessageObject([
      {fieldName: 'displayName', type: 'required'},
      {fieldName: 'email', type: 'required'},
      {fieldName: 'email', type: 'email'}
    ]);
  }

}
