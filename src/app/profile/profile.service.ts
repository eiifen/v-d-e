import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { APP_CONST } from '../shared/app.constant';
import * as service from '../services/firebase.service';

let { PROFILE_ICON_PATH } = APP_CONST;

@Injectable()
export class ProfileService {

  user$: Observable<any>;

  constructor(private firebaseService: service.FirebaseService) { }

  getData(): void {
    this.user$ = this.firebaseService.user$.map(user => this.getProfile(user));
  }  

  getProfile(user) {
    return {
      photoURL: this.getPhotoUrl(user),
      displayName: this.getDisplayName(user),
      email: this.getEmail(user)
    }
  }

  /* HELPERS */
  
  getDisplayName(user): string {
    if (!user) return '';
    return user.displayName || 'unknown';
  }

  getPhotoUrl(user): string {
    if (!user) return '';
    return user.photoURL || PROFILE_ICON_PATH;
  }

  getEmail(user): string {
    if (!user) return '';
    return user.email;
  }

}