import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileComponent } from './profile.component';
import { SharedModule } from '../shared/shared.module';
import { ProfileToolbarModule } from './profile-toolbar/profile-toolbar.module';
import { ProfileUpdateDialogModule } from './profile-update-dialog/profile-update-dialog.module';

const routes: Routes = [
  { path: '',  component: ProfileComponent }  
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes), 
    ProfileUpdateDialogModule,
    ProfileToolbarModule   
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
