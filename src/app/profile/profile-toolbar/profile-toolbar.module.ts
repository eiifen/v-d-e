import { NgModule } from '@angular/core';
import { ProfileToolbarComponent } from './profile-toolbar.component';
import { SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [SharedModule],
  exports: [ProfileToolbarComponent],
  declarations: [ProfileToolbarComponent]
})
export class ProfileToolbarModule { }
