import { Component, ViewChild, Input, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/subscription';
import { ProfileService } from './../profile.service';

@Component({
  selector: 'vde-profile-toolbar',
  templateUrl: './profile-toolbar.component.html',
  styleUrls: ['./profile-toolbar.component.scss'],
  providers: [ProfileService]
})
export class ProfileToolbarComponent {
  
  @Input() profile;
  @ViewChild('photoURL') photoURL: ElementRef;  
}
