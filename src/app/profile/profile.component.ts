import { Component, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/subscription';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';

import { IProfile } from './profile.model';
import * as service from '../services/services.barrel';
import { ProfileService } from './profile.service';
import { ProfileUpdateDialogComponent } from './profile-update-dialog/profile-update-dialog.component';

@Component({
  selector: 'vde-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [ProfileService]
})
export class ProfileComponent {

  form: FormGroup;
  user$: Observable<any>;  
  isBusy = false;
  profile$: Observable<any>;
  profile;
  translations$: Observable<any>;
  translations = {};

  constructor(
    private formBuilder: FormBuilder,
    private matDialog: MatDialog,
    private snackBarService: service.SnackBarService,
    private translationsService: service.TranslationsService,
    private ngZone: NgZone,
    private profileService: ProfileService) {
    /* this.createForm(null); */
    this.getProfile();
    this.getTranslations();
  }

  getProfile(): void {
    this.isBusy = true;
    this.user$ = 
      this.profileService
          .user$
          .do( profile => {
            this.isBusy = false;
            this.profile = profile;
          });
  }

  createForm(model: IProfile): void {
    this.form = this.formBuilder.group({
      displayName: [model ? model.displayName : '', Validators.required],
      email: [model ? model.email : '', Validators.required],
      phoneNumber: [model ? model.phoneNumber : '', Validators.required]
    });
  }

  openFieldUpdateDialog(field: string, value: string): void {
    let matDialogRef = this.matDialog.open(ProfileUpdateDialogComponent, {
      width: '80vw',
      data: {
        field,
        value
      },
    });

    matDialogRef.afterClosed().subscribe(
      () => {
        this.snackBarService.open('success');
      }
    );
  }

  getTranslations(): void {
    this.translations$ = 
      this.translationsService
          .translations$
          .do(translations => {
            this.translations = translations;
          })
  }

}
